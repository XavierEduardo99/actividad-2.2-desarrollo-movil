
# # Actividad 2.2 - Desarrollo Móvil  (Javier Vintimilla)

Actividad 2.2: Realice la actividad de Caso de estudio descrita al final de la unidad 5, sobre el desarrollo de una aplicación móvil en Flutter

## Versiones de software utilizadas:
- Flutter 3.16.5
- Android Studio Giraffe | 2022.3.1 Patch 3
- Runtime version: 17.0.6+0-b2043.56-10027231 amd64
- VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
- Android SDK: 34


Este proyecto es una demostración de como consumir una API y presentar sus contenidos utilizando el framework de desarrollo Flutter.