class User{
  String ?nombre;
  String ?usuario;
  String ?correo;
  String ?telefono;
  String ?sitioWeb;

  User(this.nombre, this.usuario, this.correo, this.telefono, this.sitioWeb);

  User.fromJson(Map<String, dynamic> json){
    nombre=json['name'];
    usuario=json['username'];
    correo=json['email'];
    telefono=json['phone'];
    sitioWeb=json['website'];
  }
}

// Realizado por: Javier Vintimilla (100-RED) 12 de enero de 2024