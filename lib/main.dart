import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'entidades/user.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueAccent),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Actividad 2.2 - Javier Vintimilla'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<User> _usuarios = <User>[];

  Future<List<User>> obtenerUsuarios() async {
    var urlUsuarios = 'https://jsonplaceholder.typicode.com/users/';
    var response = await http.get(Uri.parse(urlUsuarios));
    var usuarios = <User>[];

    if (response.statusCode == 200) {
      var usuariosJson = json.decode(response.body);
      for (var usuario in usuariosJson) {
        usuarios.add(User.fromJson(usuario));
      }
    }
    return usuarios;
  }

  @override
  void initState() {
    obtenerUsuarios().then((value) {
      setState(() {
        _usuarios.addAll(value);
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: [
                Icon(Icons.person), // Add an icon here
                SizedBox(width: 10), // Add some space between icon and text
                Text(
                  'Listado de usuarios',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Card(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Nombre: ${_usuarios[index].nombre ?? 'Sin nombre'}',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17),
                        ),
                        Text(
                            'Usuario: ${_usuarios[index].usuario ?? 'Sin usuario'}'),
                        Text(
                            'Email: ${_usuarios[index].correo ?? 'Sin correo electrónico'}'),
                        Text(
                            'Teléfono: ${_usuarios[index].telefono ?? 'Sin teléfono'}'),
                        Text('Sitio web: ${_usuarios[index].sitioWeb ?? 'Sin sitio web'}'),
                      ],
                    ),
                  ),
                );
              },
              itemCount: _usuarios.length,
            ),
          ),
          Positioned(
            bottom: 10,
            left: 10,
            child: Container(
              padding: EdgeInsets.all(10),
              color: Colors.white,
              child: Text(
                'Realizado por: Javier Vintimilla (100-RED)',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                  color: Colors.black54,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// Realizado por: Javier Vintimilla (100-RED) 12 de enero de 2024